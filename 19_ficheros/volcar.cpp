//le preguntamos al usuario 10 nombres y los guardamos en la matriz
//y después recorremos la matriz y la guardamos en un fichero
#include <stdlib.h>
#include <stdio.h>

#define MAX 0x100
#define N 10

int main(int argc, char *argv[]){
    char nombre[N][MAX]; //MAX --> longitud de las palabras

    printf("\nEscribe 10 nombres\n");
    for(int i=0; i<N; i++){
        printf(" %i.Nombre: ", i+1);
        scanf(" %s", nombre[i]);
    }

    printf("\n\n");

    for(int i=0; i<N; i++)
      printf(" %s \n", nombre[i]);

    FILE * tuberia;

    if( !( tuberia = fopen(argv[1], "w+"))){
          fprintf(stderr, "No se ha podido abrir el fichero.\n");
         return EXIT_FAILURE;
    }

    for(int i=0; i<N; i++)
          fprintf(tuberia," %s\n", nombre[i]);
    fclose(tuberia);

    printf("\n");
    printf("Se ha creado el archivo %s:\n", argv[1]);


            return EXIT_SUCCESS;
}
