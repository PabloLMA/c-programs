#include <stdio.h>
#include <stdlib.h>

#define DIM 15
#define FILENAME "fibonacci.dat"

int main(int argc, char *argv[]){

    int fibonacci[]={
        1, 1, 2, 3, 5,
        8, 13, 21, 34, 55,
        89, 144, 233, 377, 610
    };
  FILE *pf; //pf nombre del fichero en el que se va a volcar
  int num_elementos = sizeof(fibonacci)/sizeof(int);
  pf = fopen (FILENAME, "wb"); // wb --> escribe en binario

  fwrite (fibonacci, sizeof(int), num_elementos, pf); //fwrite crea el fichero y/o
                                                          //lo reescribe si ya existe

  fclose(pf);
  return EXIT_SUCCESS;
}
