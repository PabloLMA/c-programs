#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

 /* Funcion punto de entrada */

int main (){

/* printf tiene escritura bufferizada */
// printf("Hola, %s. Que tal estas? %i\n", "Pepe", (int) '0');

	fprintf(stderr, "\a"); /* stderr no esta bufferizado */
	usleep(100000);
	fputc('\a', stderr);
	usleep(100000);
	printf("\a\n");

	return EXIT_SUCCESS;
}
