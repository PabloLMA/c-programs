#include <stdio.h>
#include <stdlib.h>

#define M 2
#define K 4
#define N 3

int main(){

    int f, c;
    double A[M][K] = { //Dimensiones de la matriz
                        {2, 3, 5, 1},
                        {3, 1, 4, 2}
                     },
           B[K][N] = {
                        {5, 2, 1},
                        {3, -7, 2},
                        {-4, 5, 1},
                        {2, 3, -9}
                     },
           C[M][N];

   /* printf ("Fila: ");
    scanf (" %i", &f);
    printf ("Columna: ");
    scanf (" %i", &c);*/

    for (int i=0; i<M; i++) //filas
        for(int j=0; j<N; j++){ //columnas
          C[i][j] = 0; //pone C[i][j] a 0 porque no le hemos dado un valor previamente
           for (int k=0; k<K; k++)
              C[i][j] += A[i][k] * B[k][j];
        }
          //el bucle se mueve por las posiciones de la matriz que dentro tienen datos
          //opera lo que hay dentro de las posiciones
    for (int i=0; i<M; i++){
        for (int j=0; j<N; j++)
          printf ("\t%.2lf", C[i][j]);
    printf("\n");
    }

    return EXIT_SUCCESS;
}
