#include <stdio.h>
#include <stdlib.h>

#define M 2
#define K 4
#define N 3

void imprime (double m[][N]){ //funcion que no devuelve nada (printf) hacer llamada
   for (int i=0; i<M; i++){
       for (int j=0; j<N; j++)
         printf ("\t%.2lf", m[i][j]);
     printf("\n");
   }
}

int suma(int op1, int op2){ //declaracion y definicion de la funcion
    return op1 + op2;
}

int main(){

    int f, c, r;
    double A[M][K] = { //Dimensiones de la matriz
                        {2, 3, 5, 1},
                        {3, 1, 4, 2}
                     },
           B[K][N] = {
                        {5, 2, 1},
                        {3, -7, 2},
                        {-4, 5, 1},
                        {2, 3, -9}
                     },
           C[M][N];

    for (int i=0; i<M; i++)
        for(int j=0; j<N; j++){
          C[i][j] = 0; //pone C[i][j] a 0 porque no le hemos dado un valor previamente
           for (int k=0; k<K; k++)
              C[i][j] += A[i][k] * B[k][j];
        }
          //el bucle se mueve por las posiciones de la matriz que dentro tienen datos
          //opera lo que hay dentro de ls posiciones

    imprime (C); //llamada a la función del principio e imprime en vez de ponerla aqui
    r = suma (2,3); //"" da estos valores a la función y en vez del valor devuelve el resultado
    printf("%i\n", suma (2,3));
    return EXIT_SUCCESS;
}
