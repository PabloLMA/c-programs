#include <stdio.h>
#include <stdlib.h>

#define M 2
#define K 4
#define N 3

void imprime (double *m, int f, int c, const char *titulo){
   printf ("\n");
   printf("%s:\n", titulo);// %s cadena de caracteres
   for (int i=0; i<f; i++){
       for (int j=0; j<c; j++)
         printf ("\t%7.2lf", *(m + i * c + j));//%7.2lf- 7 caracteres delante y 2 detras(negativos)
     printf("\n");            //esto de arriba es la formula de direccionamiento de la matriz
   }
   printf ("\n");
}

int suma(int op1, int op2){ //declaracion y definicion de la funcion
    return op1 + op2;
}

int main(){

    int f, c, r;
    double A[M][K] = { //Dimensiones de la matriz
                        {2, 3, 5, 1},
                        {3, 1, 4, 2}
                     },
           B[K][N] = {
                        {5, 2, 1},
                        {3, -7, 2},
                        {-4, 5, 1},
                        {2, 3, -9}
                     },
           C[M][N];

    for (int i=0; i<M; i++)
        for(int j=0; j<N; j++){
          C[i][j] = 0; //pone C[i][j] a 0 porque no le hemos dado un valor previamente
           for (int k=0; k<K; k++)
              C[i][j] += A[i][k] * B[k][j];
        }
          //el bucle se mueve por las posiciones de la matriz que dentro tienen datos
          //opera lo que hay dentro de ls posiciones

    imprime ((double *) A, M, K, "Matriz A");// ""lo que queda ahi es la direccion de memoria de la m
    imprime ((double *) B, K, N, "Matriz B");
    imprime ((double *) C, M, N, "Matriz C");

    return EXIT_SUCCESS;
}
