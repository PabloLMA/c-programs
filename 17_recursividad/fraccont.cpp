#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define E 0.000001

double fc (int base, int prof) {
    if (prof == 1)
        return base;
    return base + 1. / fc (base, prof - 1);
}

int  main(int argc, char *argv[]){

    int base, i;
    double res0 = 0, res1=-100;

    printf ("Fracción continua del número : ");
    scanf (" %i", &base);

    for (i=1; fabs(res1-res0)>E && i<20; i++){
            res0 = res1;
            res1 = fc (base, i);
        }

    printf ("fracción Continua (%i, %i) = %.8lf\n", base, i, res1);
    return EXIT_SUCCESS;
}
