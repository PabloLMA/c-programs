//Preguntar una frase al usuario
//Alojarla en memoria (malloc)
//Mover cada letra 3 posiciones
//Imprimir por pantalla
//Liberar memoria

#include <stdio.h>
#include <stdlib.h>
#include <string.h> //tiene una funcion que lee la longitud del texto llamada strlen(sin /0)

int  main(int argc, char *argv[]){
    char *frase;
    //char frase[] = "Atacad a los galos";
    //char *p;//tiene la direccion de un caracter

   // p = frase;//guardo en p la direecion de la frase(A)
    //p = &frase[0] (mismo resultado pero no lo mismo)
    printf ("Cuales son tus ordenes Julio?");
    scanf (" %m[^\n]", &frase);//guarda el string en memoria //[^\n] coje todo menos \n

    for (char *p=frase; *p!='\0'; p++)
        *p += 3;

    printf ("%s\n", frase);

    /* MEDIANTE UN WHILE
     *
     * while (*p != '\0'){
        printf ("%c", *p+3); //imprime aquello a donde apunta p + 3
        p++;

        printf ("%c\n", frase);
    }*/

    printf("\n");
    free (frase);


    return EXIT_SUCCESS;
}

