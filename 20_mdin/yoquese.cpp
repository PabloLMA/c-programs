#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define N 0x100

int main(int argc, char *argv[]){
    char buffer[N];
    int len;
    char **palabra;//los punteros son como array(array de array de caracteres)

    for(int p=0;p<2;p++){
    palabra = (char **) realloc(palabra, p+1 * sizeof(char *));
    printf ("Palabra: ");
    fgets (buffer, N-1, stdin);//lee desde la entrada estandar,
                               //n-1 caracteres y lo guarda en la direccion del buffer
    len = strlen(buffer);//len vale la longitud(strlen) de lo que haya en buffer
    palabra[p] =(char *) malloc (len);//convertimos la direccion sin mas(malloc len) en un char*
                      //palabra contiene la direccion de la primera celda que ha reservado el malloc
    strncpy (palabra[p], buffer, len);
    palabra[p][len-1] = '\0';

    }

    for (int p=0; p<2;p++)
        printf("%s\n",palabra[p]);

    for(int p=0;p<2; p++)
        free (palabra[p]);

    free (palabra);


    return EXIT_SUCCESS;
}
