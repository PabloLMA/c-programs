#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

int main(int argc, char *argv[]){

    char **lista;
    char buffer[N];
    int len;

    printf ("Dime tu nombre: ");
    scanf (" %s", buffer);
    len = strlen (buffer);// en la variable len guardo lo que ocupe la palabra metida en buffer
    lista = (char **) malloc (3*sizeof (char *));//(malloc(funcion que reserva en memoria))
                                               //celda 0 de lista
                                               //2*sizeof -> guarda dos veces el espacio del char*
    lista[0] = (char *) malloc (len + 1);//en esa celda 0 de lista voy
                                         //a guardar la direccion de un char que tendrá
                                         //de longitud len + 1 (el valor de len+1 -> buffer)
    strncpy (lista[0], buffer, len+1);//(destilo y origen) copia en la direccion ...
                                 // len+1 --> copia las celdas justas


    //Sustituye lo que habia en el buffer pero no lo que existe en lista[0] ya que va a lista[1]
    printf ("Dime tu nombre: ");
    scanf (" %s", buffer);
    len = strlen (buffer);
    lista = (char **) malloc (3*sizeof (char *));
    lista[1] = (char *) malloc (len + 1);
    strncpy (lista[1], buffer, len+1);
    lista[2] = NULL;

    for (char **palabra = lista; *palabra != NULL; palabra++)
        printf(" %s\n", *palabra);

    free(lista[1]);//liberar el espacio de memoria
    free(lista[0]);
    free(lista);

//debugguear --> g++ -g -o lista lista.cpp
//gdb -q lista
//break main
//run
    return EXIT_SUCCESS;
}
