#include <stdio.h>
#include <stdlib.h>

#define N 10

int main(){
    int lista[N];
    for (int i=0; i<N; i++)
        lista[i] = i + 1;// esta es la correccion realmente

    for (int i=0; i<N; i++){
        for (int j=0; j<lista[i]; j++)
            printf ("*");
        printf ("\n");
    }

    return EXIT_SUCCESS;
}
