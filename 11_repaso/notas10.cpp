#include <stdio.h>
#include <stdlib.h>
#define N 10

int main(){
    int notas[N];
    double resultado; // otro tipo de variable para decimales

    for (int num=0; num<N; num++){
        printf("%i.- Niño, ¿Que nota has sacado? ", num+1);
        scanf(" %i", &notas[num]);
     }

    for (int num=0; num<N; num++)
        resultado = resultado+notas[num]; //notas[num] --> hacer referencia a la posicion del array

    printf(" %.2lf\n", resultado/10); // .2lf dos decimales

    return EXIT_SUCCESS;
}






