#include <stdio.h>
#include <stdlib.h>

int main (){

	int dia, mes, annio;
	int hdia, hmes, hannio; 
	int vividos;

	printf("Nacimiento dd/mm/aaaa: ");
	scanf("%i/%i/%i", &dia, &mes, &annio);
	printf("Fecha de hoy dd/mm/aaaa: ");
	scanf("%i/%i/%i", &hdia, &hmes, &hannio);

	vividos = (hdia-dia) + (hmes-mes)*30 + (hannio-annio)*365;
	printf("Dias vividos: %i\n", vividos);

return EXIT_SUCCESS;
}
