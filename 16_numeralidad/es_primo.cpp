#include <stdio.h>
#include <stdlib.h>

char const *program_name;
void print_usage (int exit_code) {
    FILE *f = stdout;
    if (exit_code != 0)
        f = stderr;
    fprintf (stderr, "%s <numero>\n", program_name);
    exit (exit_code);
}

bool es_primo(int pprimo){
    bool primo = true;

    for (int d=pprimo/2; primo && d>1;d--)
         if (pprimo % d == 0)
             primo = false;

    return primo;
}

/*Funcion punto de entrada*/
int main(int argc, char *argv[]){ //cada celda tiene un char *
    program_name = argv[0];
    if (argc < 2)
        print_usage (1);

    int n = atoi (argv[1]);// ./es_primos 12<--(atoi(argv[1]))

   printf("%s es primo el %i.\n", //la coma separa parametros
           es_primo (n)? "Sí": "No",
           n);

    return EXIT_SUCCESS;
}
