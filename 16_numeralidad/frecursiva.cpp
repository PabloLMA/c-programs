#include <stdlib.h>
#include <stdio.h>

double fc (int base, int prof){
  if (prof == 1)
    return base;
  return base + 1. / fc (base,prof - 1);
}

//f(2,V) = 2 +1 / f(2, V-1)
int main(int argc, char *argv[]){

  int base, prof;
  double res;

  printf ("Fraccion continua del numero: ");
  scanf(" %i", &base);
  printf ("Profundidad de Cálculo: ");
  scanf (" %i", &prof);

  res = fc (base, prof);
  printf ("fraccion Continua (%i, %i) = %.2lf\n", base, prof, res);
  return EXIT_SUCCESS;
}
