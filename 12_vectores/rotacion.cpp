#include <stdio.h>
#include <stdlib.h>
#define I 0
#define J 0
int main(){
    double a[I][J],
    resultX,
    resultY,
    angulo,
    rsen,
    rcos;

      printf("Dime las coordenadas X , Y: ");
      scanf(" %lf, %lf", &resultX, &resultY);


      printf("Dime el ángulo: ");
      scanf(" %lf", &angulo);

      rsen = (sin(angulo * 2* 3.14159 / 360.0));
      rcos = (cos(angulo * 3.14159 / 180));

      printf("Seno del angulo: %lf\n", rsen);
      printf("Coseno del angulo: %lf \n", rcos);

return EXIT_SUCCESS;
}
