#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    double Vector[2], Vector2[2], VectorTotal[2], angulo;
    printf("Vector: ");
    scanf(" %lf, %lf", &Vector[0], &Vector[1]);

    printf("Vector2: ");
    scanf(" %lf, %lf", &Vector2[0], &Vector2[1]);

    VectorTotal[0] = Vector[0] + Vector2[0];
    VectorTotal[1] = Vector[1] + Vector2[1];

    angulo = atan2(VectorTotal[0], VectorTotal[1]);
    printf(" %lf \n", angulo * 180/M_PI);

    return EXIT_SUCCESS;
}
