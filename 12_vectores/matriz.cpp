#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define I 3
#define J 3

int main(){
    double a[I][J],
        resultadoP,
        resultadoN;
    for(int i=0;i<3;i++){
        printf("Dime el vector: %i\n", i);
        scanf(" %lf, %lf, %lf", &a[i][0], &a[i][1], &a[i][2]);
    }

        printf("\n\n");

    resultadoP = (a[0][0]*a[1][1]*a[2][2])+
                 (a[1][0]*a[2][1]*a[0][2])+
                 (a[2][0]*a[0][1]*a[1][2]);
    printf("El resultado de la suma es: %.2lf\n", resultadoP);

    resultadoN = (a[0][2]*a[1][1]*a[2][0])-
                 (a[1][2]*a[2][1]*a[0][0])-
                 (a[2][2]*a[0][1]*a[1][0]);
    printf("El resultado de la resta es: %.2lf\n\n\n", resultadoN);
    //DATOS DEL ARRAY

    printf("DATOS DEL ARRAY:\n");
    for (int i=0;i<3;i++)
        printf(" %.2lf, %.2lf, %.2lf\n", a[i][0], a[i][1], a[i][2]);

    return EXIT_SUCCESS;
}
