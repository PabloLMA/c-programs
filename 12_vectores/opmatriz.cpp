#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define I 2
#define J 3

#define F 3
#define C 1
int main(){
    double a[I][J],
    b[F][C],
    resultado1,
    resultado2;

    for(int i=0;i<2;i++){
        printf("Dime el vector tridimensional: %i\n", i);
        scanf(" %lf, %lf, %lf", &a[i][0], &a[i][1], &a[i][2]);
    }

    printf("\n\n");

    for(int i=0;i<3;i++){
        printf("Dime el vector de unidimensional: %i\n", i);
        scanf(" %lf", &b[i][0]);
    }

    printf("\n\n");

    // resultado = (a[I][0]*b[0][C]+a[I][1]*b[1][C]+a[I][2]*b[2][C]);
    resultado1 = (a[0][0]*b[0][0]+a[0][1]*b[1][0]+a[0][2]*b[2][0]);
    resultado2 = (a[1][0]*b[0][0]+a[1][1]*b[1][0]+a[1][2]*b[2][0]);
    printf("El resultado1 de la operacion es: %.2lf\n\n", resultado1);
    printf("El resultado2 de la operacion es: %.2lf\n\n", resultado2);

    printf("DATOS DEL PRIMER ARRAY:\n");
      for(int i=0;i<2;i++){
        printf(" %.2lf, %.2lf, %.2lf\n", a[i][0], a[i][1], a[i][2]);
      }
    printf("\n\n");

    printf("DATOS DEL SEGUNDO ARRAY:\n");
      for(int i=0;i<3;i++){
        printf(" %.2lf\n", b[i][0]);
      }

    return EXIT_SUCCESS;
}
