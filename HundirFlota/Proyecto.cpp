#include <stdio.h>
#include <stdlib.h>
#include <ctime> //para el random
#include <strings.h>

#define F 8 //TableroProgramador
#define C 8
#define FU 18 //TableroUsuario (Fila Usuario)
#define CU 18
#define N 8
/*const char *simbolo[] = {"〜", //u+25A0
                             "-■"};*/
const char *simbolo[] = {"#", //u+25A0
                             "&"};

void imprime (char TableroProgramador[F][C]){
for (int i=0; i<F; i++){
  for (int j=0; j<C; j++)
    printf ("%c ", TableroProgramador[i][j]);
      printf("\n");
  }
}
  void imprime2 (char TableroProgramador[F][C]){
    for(int f=0; f<F;f++){
        for(int c=0; c<C;c++){
            if(TableroProgramador[f][c] == 'H')
                printf(" %s", simbolo[1]);
            else
                printf(" %s", simbolo[0]);
        }
        printf("\n");
      }
  }



int main(){
    char TableroProgramador[F][C] = { // Si hay dos jugadores es el de J1
        {'X','X','X','X','X','X','X','X'},//00-01-02-03-04-05-06-07
        {'X','X','X','X','X','X','X','X'},//10-11-12-13-14-15-16-17
        {'X','X','X','X','X','X','X','X'},//00-01-02-03-04-05-06-07
        {'X','X','X','X','X','X','X','X'},//30-31-32-33-34-35-36-37
        {'X','X','X','X','X','X','X','X'},//00-01-02-03-04-05-06-07
        {'X','X','X','X','X','X','X','X'},//10-11-12-13-14-15-16-17
        {'X','X','X','X','X','X','X','X'},//00-01-02-03-04-05-06-07
        {'X','X','X','X','X','X','X','X'} //70-71-72-73-74-75-76-77
    };

    int fila,
        columna,
        longPortaaviones = 5,
        longAcorazado = 4,
        longCrucero = 3,
        longDestructor = 2,
        longSubmarino = 1,
        direccion = 0,
        a[N][N];

        bool endR = false; // Condicion del random (bool --> verdadero o falso

    srand(time(NULL)); // semilla para el random

    printf("\n");
    printf("Bienvenido a Hundir la flota :)\n\n");

    getchar();
    /*system("clear");*/
    system("cls");


    printf("Bueno, aqui tienes el tablero donde vas a hundir los barcos de la  flota\n\n");
    printf("Tu objetivo sera hundir:\n\n"
            "1 Portaaviones de 5 casillas de longitud\n"
            "1 Acorazado de 4 casillas de longitud\n"
            "1 Cruzero de 3 casillas de longitud\n"
            "1 Destructor de 2 casillas de longitud\n"
            "1 Submarino de 1 casilla de longitud\n\n");

    imprime2 (TableroProgramador);

    getchar();
    /*system("clear");*/
    system("cls");

    //Generar Portaaviones random
    while(endR == false){

        int X = rand() % F; //Random de X(Filas)
        int Y = rand() % C; //Random de Y(Columnas)
        bool Choque = false; //Condicion de que no choque

        //Primer Barco(Portaaviones)
         if(direccion == 0){ //Esto eligue que Barco cojer

             if(X <= 3){ //Condicion para que no se pase Y como es el primero no hace falta noChoque
                for(int i=0; i<longPortaaviones; i++)
                   TableroProgramador[X+i][Y] = 'O'; //Vertical
                direccion++;
               }
        }
        //Segundo Barco(Acorazado)
          else if(direccion == 1){

            if(Y <= 4){
              for(int j=0; j<longAcorazado; j++)
                  if(TableroProgramador[X][Y+j] != 'X') //Comprueba si choca
                      Choque = true; //Si choca deja continuar

              if(Choque == false){
                  for(int i=0; i<longAcorazado; i++)
                      TableroProgramador[X][Y+i] = 'O'; //Horizontal
                    direccion++;
                   }
             }
        }

        //Tercer Barco(Crucero)
         else if(direccion == 2){

            if(Y <= 4){
              for(int j=0; j<longCrucero; j++)
                  if(TableroProgramador[X][Y+j] != 'X') //Comprueba si choca
                      Choque = true; //Si choca deja continuar

              if(Choque == false){
                  for(int i=0; i<longCrucero; i++)
                      TableroProgramador[X][Y+i] = 'O'; //Horizontal
                    direccion++;

                   }
             }
        }

         //Cuarto Barco(Destructor)
         else if(direccion == 3){

            if(Y <= 4){
              for(int j=0; j<longDestructor; j++)
                  if(TableroProgramador[X][Y+j] != 'X') //Comprueba si choca
                      Choque = true; //Si choca deja continuar

              if(Choque == false){
                  for(int i=0; i<longDestructor; i++)
                      TableroProgramador[X][Y+i] = 'O'; //Horizontal
                    direccion++;
                   }
             }
        }

         //Quinto Barco(Submarino)
         else if(direccion == 4){

            if(Y <= 4){
              for(int j=0; j<longSubmarino; j++)
                  if(TableroProgramador[X][Y+j] != 'X') //Comprueba si choca
                      Choque = true; //Si choca deja continuar

              if(Choque == false){
                  for(int i=0; i<longSubmarino; i++)
                      TableroProgramador[X][Y+i] = 'O'; //Horizontal
                    endR = true;
                   }
             }
        }
    }
        //Ataque a la flota

        for (int p=0; p<30; p++){
            bool ganar = true;
            printf("A continuacion,\n"
                    "elige dónde quieres lanzar tu ataque escribiendo primero la fila,\n"
                    "seguido de una coma y posteriormente la columna: ");
            scanf(" %i, %i", &fila, &columna);
            printf("\n");

            /*system("clear");*/
              system("cls");

            if (TableroProgramador[fila][columna] == 'O'){
                    TableroProgramador[fila][columna] = 'H';

                   printf("Tocado\n\n");
            }

            else if(TableroProgramador[fila][columna] == 'H'){
                printf("Ya le habías dado\n");}
              else{
                  printf("Fallaste\n\n");
              }

            /*imprime (TableroProgramador);
            printf("\n\n");*/

            imprime2 (TableroProgramador);
            printf("\n\n");

            for(int i=0; i<F; i++){
              for(int j=0; j<C; j++){
                if(TableroProgramador[i][j] == 'O'){
                    ganar = false;
                    break;
                }
              }
            }
            if( ganar == true){
                printf("Ganaste!!\n");
                break;
            }
        }


return EXIT_SUCCESS;

}
