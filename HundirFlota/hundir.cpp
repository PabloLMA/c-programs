#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <time.h>

#define D  8
#define DR 4
#define TINICIAL 20

#define MAX 0x100

enum TBarcos {
    agua,
    lancha,
    fragata,
    submarino,
    acorazado,
    portaaviones,
    TOTAL
};

const char *simbolo[] = {"♒", "🏄", "🚣", "🚤", "⛵", "🚢"};
const char *nombre[] = {
    "Agua",
    "Surfero",
    "Tonto del Bote",
    "Yate",
    "Velero",
    "Transatlántico"
};

char mssg[MAX];

int dirs[DR][2] = {
    { 0, 1},
    { 1, 0},
    { 1, 1},
    {-1, 1}
};

void elegir (int *f0, int *c0, int barco, int dir) {
        /* Resolver el problema de la f y col inicial */
        if (dirs[dir][0] > 0)
            *f0 = rand () % (D + 1 - barco * dirs[dir][0]);
        else {
            int nc = D + 1 - barco;
            *f0 = rand () % nc + D - nc;
        }
        *c0 = rand () % (D + 1 - barco * dirs[dir][1]);
}

void rellenar (int tablero[D][D]) {
    bzero (tablero, D * D * sizeof(int));
    int f0, c0, dir, suma;

    for (int barco=lancha; barco<TOTAL; barco++){
        dir = rand () % DR;
        do {
            elegir (&f0, &c0, barco, dir);
            suma = 0;   // Vamos a ver si esta posición está libre.
            for (int k=0; k<barco; k++)
                suma += tablero[f0 + k * dirs[dir][0]][c0 + k * dirs[dir][1]];
        } while (suma != 0);

        for (int k=0; k<barco; k++)
            tablero[f0 + k * dirs[dir][0]][c0 + k * dirs[dir][1]] = barco;
    }
}

/*
 * Elimina el barco que en la fila fila, columna col.
 * Devuelve: la cantidad de casillas con barco
 */
int cambiar (int tablero[D][D], int fila, int col){
    int suma = 0;

    switch (tablero[fila][col]){
        case agua:
            sprintf (mssg, "Agua.\n");
            break;
        default:
            sprintf (mssg, "Le has dado al %s.\n", nombre[ tablero[fila][col] ]);
    }

    tablero[fila][col] = 0;
    for (int f=0; f<D;f++)
        for (int c=0; c<D; c++)
            suma += tablero[f][c];

    return suma;
}

void titulo () {
    system ("clear");
    system ("toilet -fpagga HUNDIR LA FLOTA");
    printf ("\n\n\n");
}

void imprimir (int tablero[D][D], int tiros){
    for (int f=0; f<D; f++){
        printf ("\t");
        for (int c=0; c<D; c++)
            printf ("%s", simbolo[ tablero[f][c] ]);
        printf ("\n");
    }

    printf ("\n%s\n", mssg);
    printf ("Te quedan: %i tiros.\n\n", tiros);
}

void pedir_coord (int *fila, int *col, int *tiros){
    printf ("· Fila: ");
    scanf (" %i", fila);
    printf ("· Columna: ");
    scanf (" %i", col);
    (*fila)--;
    (*col)--;
    (*tiros)--;
}

/* Función punto de entrada */
int  main(){
    int tiros = TINICIAL,
        fila,
        col,
        tablero[D][D];

    srand(time(NULL));
    rellenar(tablero);

    do {
        titulo ();
        imprimir (tablero, tiros);
        pedir_coord(&fila, &col, &tiros);
    } while (cambiar(tablero, fila, col) && tiros > 0);

    return EXIT_SUCCESS;
}
