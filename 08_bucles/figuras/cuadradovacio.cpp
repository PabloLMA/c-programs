#include <stdio.h>
#include <stdlib.h>

int main (){
    int l = 5;
     for (int f=0; f<l; f++){   //muevo posiciones
        for (int c=0; c<l; c++) //muevo posiciones
            if (f==0 || f==l-1) // || --> o, relleno la fila si es 0 o 4
            printf ("*");
            else // else if porque sino cumple else que es SINO
                if (c==0 || c==l-1) // relleno la columna si es 0 o 4
                    printf("*");
                else
                    printf(" "); // relleno el resto de espacios en blanco

        printf ("\n");
    }

    return EXIT_SUCCESS;
}
