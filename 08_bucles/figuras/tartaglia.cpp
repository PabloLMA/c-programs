#include <stdio.h>
#include <stdlib.h>
#define N 13

int main(){
    unsigned tarta[N][N];

    for(int fila=0; fila<N; fila++){
        for(int col = 0; col<=fila; col++){
            if(col == fila || col == 0)
                tarta[fila][col] = 1;
            else
                tarta[fila][col]= tarta[fila-1][col] + tarta[fila-1][col-1];

            printf(" %3d", tarta[fila][col]);
        }
        printf("\n");
    }

    return EXIT_SUCCESS;
}
