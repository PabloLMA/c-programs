#include <stdio.h>
#include <stdlib.h>

#define N 15

int main(){

    int emento[N];
/* Condiciones de contorno */
    emento[1] = emento[0] = 1;
/* calculos */
  for (int i=2; i<N; i++)               //el valor de i deja de existir al
                                        //salir del bucle
  emento[i] = emento[i-1] + emento[i-2];
/* salida de datos */
  for (int i=0; i<N; i++)
      printf ("%i ", emento[i]);
 printf ("\n");

 for (int i=1; i<N; i++)
     printf ("%lf ", (double) emento[i] / emento [i-1]); //long float-decima
  printf ("\n");  //double para poner decimales, int no los pone
  /*
  printf ("%i ", emento[0]);
  printf ("%i ", emento[1]);
  printf ("%i ", emento[i]);*/

    return EXIT_SUCCESS;
}
