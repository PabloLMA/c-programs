//Programa que te diga cuantos dias llevas vividos

#include <stdio.h>
#include <stdlib.h>

int main(){
	int dia, mes, annio;    // Nacimiento
	int hdia, hmes, hannio; // hoy
	int vividos;

	printf("Nacimiento dd/mm/aaaa: ");
	scanf("%i/%i/%i", &dia, &mes, &annio);
	printf("En que dia estamos: ");
	scanf("%i/%i/%i", &hdia, &hmes, &hannio);

	vividos = (hdia-dia) +(hmes-mes)*30 + (hannio-annio)*365;
	printf("Dias vividos= %i\n", vividos);

	return EXIT_SUCCESS;
}
